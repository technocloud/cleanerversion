## 2.3.1 (2022-02-08)

- Fixed bug with m2m relations

## 2.3.0 (2021-11-23)

- Add compatibility with Django 3.2
- Pyupgrade and Blackify

## 2.2.0 (2020-02-27)

- Add compatibility with Django 2.2
- Add new properties in VersionedAdmin: list_filters_show, only_current, list_display_show_is_current
- Fix bug

